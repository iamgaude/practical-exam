from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

from django.contrib import admin

urlpatterns = patterns(
    "",
    url(r"^admin/", include(admin.site.urls)),
    url(r"", include("blogs.urls")),
    url(r'^blogs/contact/$', "blogs.views.contact", name="contact"),
    url(r'^blogs/login/$', "blogs.views.login_view", name="login"),
    url(r'^blogs/logout/$', "blogs.views.logout_view", name="logout"),
    url(r'^blogs/signup/$', "blogs.views.register", name="register"),
    url(r'^blogs/post/$', "blogs.views.post", name="post"),
    url(r'^blogs/user_posts/$', "blogs.views.blog_user_posts", name="user_posts"),
    url(r'^blogs/update_post/(?P<post_pk>\d+)/$', "blogs.views.blog_update_post", name="update_post"),
    url(r'^blogs/delete_post/(?P<post_pk>\d+)/$', "blogs.views.blog_delete_post", name="delete_post"),
)


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
